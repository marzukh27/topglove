package com.topglove.TopGloveCms;

import com.topglove.TopGloveCms.Model.AlertModel;
import com.topglove.TopGloveCms.Model.JsonModel3;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by X13 on 3/4/2017.
 */

public interface RestCrud {

    @Headers({
            "Content-Type: application/json",
            "Accept: text/plain"
    })
    @POST("RestApi/GetUser")
    Call<String> GetUser(@Body AlertModel.UserInfo userInfo);


    @GET("RestApi/GetFactory")
    Call<List<AlertModel.FactoryList>> GetFactory();

    @GET("RestApi/GetModule")
    Call<List<AlertModel.ZoneTicket>> GetModule();

    @GET("RestApi/GetPic")
    Call<List<AlertModel.ModelWorkers>> GetPic();

    @GET("RestApi/GetLine/{FactoryNameAndPic}")
    Call<List<AlertModel.LineList>> GetLine(@Path("FactoryNameAndPic") String FactoryNameAndPic);

    @GET("RestApi/GetAllLine/{FactoryName}")
    Call<List<AlertModel.LineList>> GetAllLine(@Path("FactoryName") String FactoryName);

    @GET("RestApi/GetAllCheckList/{ModuleName}")
    Call<List<AlertModel.Checklist>> GetAllCheckList(@Path("ModuleName") String ModuleName);

    @GET("RestApi/GetTicketType/{ModuleName}")
    Call<List<AlertModel.ZoneTicket>> GetTicketType(@Path("ModuleName") String ModuleName);

    @GET("RestApi/GetAllZone/{LineName}")
    Call<List<AlertModel.ZoneList>> GetAllZone(@Path("LineName") String LineName);


    @GET("RestApi/GetZone/{LineNameAndPic}")
    Call<List<AlertModel.ZoneList>> GetZone(@Path("LineNameAndPic") String LineNameAndPic);

    @GET("RestApi/GetZoneTicket/{ZoneNameAndPic}")
    Call<List<AlertModel.ZoneTicket>> GetZoneTicket(@Path("ZoneNameAndPic") String ZoneNameAndPic);

    @GET("RestApi/GetApprovalZoneTicket/{PicNameAndType}")
    Call<List<AlertModel.ZoneTicket>> GetApprovalZoneTicket(@Path("PicNameAndType") String PicNameAndType);

    @GET("RestApi/GetCheckList/{TicketId}")
    Call<List<AlertModel.ModelGetCheckList>> GetCheckList(@Path("TicketId") String TicketId);

    @GET("RestApi/ListWorkers")
    Call<List<AlertModel.ModelWorkers>> ListWorkers();

    @GET("RestApi/UpdateCheckList/{PicAndChecklistIdandStatus}")
    Call<String> UpdateCheckList(@Path("PicAndChecklistIdandStatus") String PicAndChecklistIdandStatus);


    @GET("RestApi/GetNewDownMachine/{PicNameAndPicType}")
    Call<String> GetNewDownMachine(@Path("PicNameAndPicType") String PicNameAndPicType);

    @GET("RestApi/PassOver/{TicketId}")
    Call<String> PassOver(@Path("TicketId") String TicketId);

    @Headers({
            "Content-Type: application/json",
            "Accept: text/plain"
    })
    @POST("RestApi/ACKDownTime")
    Call<String> ACKDownTime (@Body JsonModel3.ACKDownTime ackDownTime);

    @Headers({
            "Content-Type: application/json",
            "Accept: text/plain"
    })
    @POST("RestApi/CloseTicket")
    Call<String> CloseTicket (@Body JsonModel3.CloseTicketModel CloseTicketModel);

    @Headers({
            "Content-Type: application/json",
            "Accept: text/plain"
    })
    @POST("RestApi/SetApproval")
    Call<String> SetApproval (@Body JsonModel3.CloseTicketModel CloseTicketModel);


    @Headers({
            "Content-Type: application/json",
            "Accept: text/plain"
    })
    @POST("RestApi/UpdateWorkerAssignment")
    Call<String> UpdateWorkerAssignment (@Body AlertModel.WorkerAssignModel WorkerAssignModel);


    @Multipart
    @POST("RestApi/PostUserImage/{OtherParam}")
    Call<ResponseBody> PostUserImage(
            @Part MultipartBody.Part file,
            @Path("OtherParam") String OtherParam
    );



}
