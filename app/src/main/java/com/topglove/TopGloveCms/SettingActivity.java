package com.topglove.TopGloveCms;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class SettingActivity extends AppCompatActivity implements
        View.OnClickListener {

    private Intent intent;
    private Vibrator vibrator;

    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String URL = "URL";
    EditText txtURL;
    Button b1,b2;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        this.setContentView(R.layout.activity_setting);
        vibrator = (Vibrator)this.getSystemService(Context.VIBRATOR_SERVICE);
        txtURL=(EditText)findViewById(R.id.txtURL);


        b1=(Button)findViewById(R.id.btn_SettingSave);
        b2=(Button)findViewById(R.id.btnSettingBack);
        b1.setOnClickListener(this);
        b2.setOnClickListener(this);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);


        String restoredText = sharedpreferences.getString(URL, null);
        txtURL.setText(restoredText);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_SettingSave:
                 editor = sharedpreferences.edit();
                 editor.putString(URL, txtURL.getText().toString());
                 editor.commit();
                APIClient.URL = "http://"+txtURL.getText().toString();
                Toast.makeText(this, "Saved....", Toast.LENGTH_LONG).show();
                break;
            case R.id.btnSettingBack:

                finish();
                break;
        }

    }

            //We will use this method to build our URL string and then pass it to the network task object
    //to execute it in the background.
    //When we receive a reply then

    }


