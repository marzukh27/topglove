package com.topglove.TopGloveCms.Model;

/**
 * Created by X-PC on 8/3/2018.
 */

public class AlertModel {

    public class UserInfo
    {
        public String Username;
        public String Password;
        public String UserType;
    }
    public class FactoryList
    {
        public String Id;
        public String FactoryName;
    }
    public class LineList
    {
        public String LineName;
        public String DownCount;
    }
    public class ZoneList
    {
        public String ZoneId;
        public String ZoneName;
        public String DownCount;
        public String Color;
    }
    public class Checklist
    {
        public String CheckListName;
    }

    public class ZoneTicket
    {
        public int Id ;
        public String TaskId ;
        public String Status ;
        public String TicketName ;
        public String ModuleName ;
        public String PIC ;
        public String IssueBy ;
        public String Color ;
        public String Code ;
        public String NeededApproval ;
        public String PlanUnplan ;
        public String Description ;
        public int TicketNumber ;
        public String TargetGroup ;
        public String StartDate ;
        public String EndDate ;
        public int TargetDuration ;
        public String ActualDuration ;
        public int Priority ;
        public String HaveChecklist ;
        public String LineStop ;
        public String DateCreated ;
        public String ZoneName ;
        public String LineName ;
        public String FactoryName ;
        public int ModuleId ;
        public int TicketTypeId ;
        public String Remedy ;
        public int ZoneId ;
        public String Remarks;
        public String Workers;
        public String WorkerName;
    }

    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    public class CountLocked {

        public int DownCount;
        public String LineName;

    }

    public class UnlockPostModel
    {
        public String Id;
        public String Pic;
        public String ToolName;
        public String RootCause;
        public String Correctiveness;
        public String Password;
        public String dtstatus;
        public String dbIsLock;
        public String dbChecklist;
    }

    public class ModelDownTime
    {
        public String Id;
        public String MachineId;
        public String MachineName;
        public String Status;
        public String LineName;
        public String DateTime;
        public String StartDate;
        public String IsLock;
        public String Checklist;
        public String EndTime;
        public String Description;
        public String DownType;
        public String LotId;
        public String Color;

    }

    public class ModelGetCheckList
    {
        public int Id;
        public int TicketId;
        public String DateCreated;
        public String EndDate;
        public int ChecklistDetailsId;
        public String Others;
        public String PIC;
        public String Checklist;

    }

    public class ModelWorkers
    {
        public int Id;
        public int NoOfTicket;
        public String FullName ;
    }

    public class WorkerAssignModel
    {
        public int Id;
        public String WorkerId;
        public String WorkerName ;
    }


    public  class ModelPassOver
    {
        public String Id;
        public String PIC;
        public String PICType;
        public String Remarks;
    }

}
