package com.topglove.TopGloveCms.Helper;

import android.app.AlertDialog;
import android.content.Context;

import com.topglove.TopGloveCms.R;

import dmax.dialog.SpotsDialog;

public class LoadingClass{

    // ProgressDialog progressDialog;
    AlertDialog progressDialog;
    Context context;

    public LoadingClass(Context context, int type){

        this.context = context;

        switch (type)
        {
            case 1:

                progressDialog = new SpotsDialog(context,"Please Wait Loading",R.style.Custom);

                break;

            case 2:

                progressDialog = new SpotsDialog(context,"Authenticating",R.style.Custom);

                break;
        }

    }

    public void show(){
        //progressDialog.setIndeterminate(true);
        //progressDialog.setMessage("Loading");
        progressDialog.show();
        progressDialog.setCanceledOnTouchOutside(false);
    }

    public void dismiss(){
        if(progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }
}
