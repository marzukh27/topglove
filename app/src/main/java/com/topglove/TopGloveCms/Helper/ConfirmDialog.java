package com.topglove.TopGloveCms.Helper;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;


import com.topglove.TopGloveCms.R;

public class ConfirmDialog extends Dialog implements
        android.view.View.OnClickListener {

    public Activity c;
    public Dialog d;
    public Button btnOk, btnCancel;

    TextView txt_dia;
    String dialogText="";

    public ConfirmDialog(Activity a,String dialogText) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        this.dialogText = dialogText;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_confirm_dialog);
        txt_dia = (TextView) findViewById(R.id.tvTittle);


        btnOk = (Button) findViewById(R.id.btnOk);
        btnCancel = (Button) findViewById(R.id.btnCancel);
        btnOk.setOnClickListener(this);
        btnCancel.setOnClickListener(this);

        txt_dia.setText(dialogText);

    }



    public int Okpressed;

    public int isOkpressed() {
        return Okpressed;
    }

    public void setOkpressed(int okpressed) {
        Okpressed = okpressed;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnOk:
                setOkpressed(1);
                dismiss();
                break;
            case R.id.btnCancel:
                setOkpressed(0);
                dismiss();
                break;
            default:
                break;
        }

    }
}
