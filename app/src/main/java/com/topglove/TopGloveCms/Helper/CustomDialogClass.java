package com.topglove.TopGloveCms.Helper;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.topglove.TopGloveCms.R;

public class CustomDialogClass extends Dialog implements
        android.view.View.OnClickListener {

    public Context c;
    public Dialog d;
    public Button btnUnlockOk, btnUnlockCancel;
    EditText tvCommentBox, tvPreventiveAction;
    TextView txt_dia;

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }
    private String Password;


    public String getRootCause() {
        return Comment;
    }
    public void setRootCause(String comment) {
        Comment = comment;
    }
    private String Comment;


    public String getCorrectiveAction() {
        return CorrectiveAction;
    }
    public void setCorrectiveAction(String correctiveAction) {
        CorrectiveAction = correctiveAction;
    }
    private String CorrectiveAction;


    public int getType() {
        return Type;
    }

    public void setType(int type) {
        Type = type;
    }

    private int Type;


    public CustomDialogClass(Context a) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.confirm_dialog);
        txt_dia = (TextView) findViewById(R.id.tvTittle);

        tvCommentBox =  (EditText) findViewById(R.id.tvCommentBox);
        //tvPreventiveAction =  (EditText) findViewById(R.id.tvCommentBox2);
        btnUnlockOk = (Button) findViewById(R.id.btnSendApproval);
        btnUnlockCancel = (Button) findViewById(R.id.btnUnlockCancel);
        btnUnlockOk.setOnClickListener(this);
        btnUnlockCancel.setOnClickListener(this);

        txt_dia.setText("Please Enter Required Information");

        if(getType() == 0){

        }
        else if(getType() == 1){
            tvCommentBox.setText("Unlocked When Attend");
            tvCommentBox.setVisibility(View.GONE);
        }
        else if(getType()==2){
            //tvUnlockPassword.setText("Unlocked When Attend");
            //tvUnlockPassword.setVisibility(View.GONE);
        }
        else if(getType()==3){
            tvCommentBox.setVisibility(View.GONE);
        }

    }



    public int Okpressed;

    public int isOkpressed() {
        return Okpressed;
    }

    public void setOkpressed(int okpressed) {
        Okpressed = okpressed;
    }






    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSendApproval:
                if(getType()==3){
                    setOkpressed(1);
                    dismiss();
                }
                else if(tvCommentBox.getText().toString().length() < 1){
                    Toast.makeText(c,"Please Enter Corrective Action",Toast.LENGTH_SHORT).show();
                }
                else if(tvPreventiveAction.getText().toString().length() < 1){
                    Toast.makeText(c,"Please Enter Preventive Action",Toast.LENGTH_SHORT).show();
                }
                else{
                    setCorrectiveAction(tvCommentBox.getText().toString());
                    setRootCause(tvPreventiveAction.getText().toString());
                    setOkpressed(1);
                    dismiss();
                }

                break;
            case R.id.btnUnlockCancel:
                setOkpressed(0);
                dismiss();
                break;
            default:
                break;
        }

    }
}
