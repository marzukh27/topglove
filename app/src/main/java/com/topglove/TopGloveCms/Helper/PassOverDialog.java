package com.topglove.TopGloveCms.Helper;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;


import com.topglove.TopGloveCms.R;


import java.util.ArrayList;
import java.util.List;

public class PassOverDialog extends Dialog implements
        android.view.View.OnClickListener {

    public Activity c;
    public Dialog d;
    public Button btnOk, btnCancel;
    RadioGroup passoverlist;
    TextView txt_dia;
    String dialogText="";

    EditText tvCommentBox;
    List<String> SelectedPic;


    public PassOverDialog(Activity a,String dialogText) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        this.dialogText = dialogText;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_passover);

        SelectedPic = new ArrayList<String>();
        passoverlist = (RadioGroup) findViewById(R.id.rgroup_passoverlist);
        btnOk = (Button) findViewById(R.id.btnOk);
        btnCancel = (Button) findViewById(R.id.btnCancel);
        tvCommentBox = (EditText) findViewById(R.id.tvCommentBox);

        btnOk.setOnClickListener(this);
        btnCancel.setOnClickListener(this);


        CheckBox rbn = new CheckBox(this.getContext());
        rbn.setId(1000+1);
        rbn.setText("MFG");
        rbn.setOnClickListener(this);
        passoverlist.addView(rbn);

        rbn = new CheckBox(this.getContext());
        rbn.setId(1000+2);
        rbn.setText("EE");
        rbn.setOnClickListener(this);
        passoverlist.addView(rbn);

        rbn = new CheckBox(this.getContext());
        rbn.setId(1000+3);
        rbn.setText("PE");
        rbn.setOnClickListener(this);
        passoverlist.addView(rbn);

        rbn = new CheckBox(this.getContext());
        rbn.setId(1000+4);
        rbn.setText("QA");
        rbn.setOnClickListener(this);
        passoverlist.addView(rbn);


        /*
        int buttons = 5;
        for (int i = 1; i <= buttons ; i++) {

        }
        */

    }



    public int Okpressed;

    public int isOkpressed() {
        return Okpressed;
    }

    public void setOkpressed(int okpressed) {
        Okpressed = okpressed;
    }

    public String getPicType() {
        return PicType;
    }

    public void setPicType(String picType) {
        PicType = picType;
    }

    private String PicType;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    private String comment="";

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnOk:

                if(SelectedPic.size()<1){
                    Toast.makeText(c.getApplicationContext(),"Please Select Group Type",Toast.LENGTH_SHORT).show();
                    return;
                }
                else{
                    String values = "";
                    for(String item : SelectedPic){
                        values = values + item +",";
                    }

                    values = values.substring(0,values.length()-1);
                    setPicType(values);

                }

                if(tvCommentBox.getText().toString().isEmpty()){
                    Toast.makeText(c.getApplicationContext(),"Please Key in Action",Toast.LENGTH_SHORT).show();
                    SelectedPic.clear();
                    return;
                }
                else if(tvCommentBox.getText().length() < 10){
                    Toast.makeText(c.getApplicationContext(),"Action must be more then 10 characters",Toast.LENGTH_SHORT).show();
                    SelectedPic.clear();
                    return;
                }
                else {
                    setComment(tvCommentBox.getText().toString());
                    setOkpressed(1);
                    SelectedPic.clear();
                    dismiss();
                }

                break;
            case R.id.btnCancel:
                setOkpressed(0);
                dismiss();
                break;

            default:
                CheckBox cbox = (CheckBox)findViewById(v.getId());

                if(cbox.isChecked()){
                    if(!SelectedPic.contains(cbox.getText().toString())){
                        SelectedPic.add(cbox.getText().toString());
                    }
                }
                else if(!cbox.isChecked()){
                    SelectedPic.remove(cbox.getText().toString());
                }


                //Toast.makeText(c.getApplicationContext(),cbox.getText(),Toast.LENGTH_SHORT).show();
                break;
        }

    }
}
