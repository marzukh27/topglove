package com.topglove.TopGloveCms;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.topglove.TopGloveCms.Helper.LoadingClass;
import com.topglove.TopGloveCms.Helper.Var;
import com.topglove.TopGloveCms.Model.AlertModel;

import java.util.List;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Machine_LineActivity extends AppCompatActivity implements View.OnClickListener {

    Button Back;
    Button btnNew,btnAssign;

    TextView lineName;
    TableLayout layoutMachineList;
    RestCrud RestCrud;
    String Page = "";
    String FactoryName="";
    String Line;
    String CurrentPage="";
    MyConfig myConfig = MyConfig.getInstance();
    AlertHelper alertHelper;
    List<AlertModel.ModelDownTime> modelDownTimes;
    boolean CreateState=true;
    LoadingClass LoadActivity;

    @Override
    public void onResume(){
        super.onResume();
        Log.d("test","ImResume");
        if(!CreateState){
            MyConfig.TaskListMachineByLine.removeCallbacks(UpdateMachine);
            MyConfig.TaskListMachineByLine.post(UpdateMachine);

            myConfig = MyConfig.getInstance();

            if(myConfig.globalpage.contains(Var.TicketNew)){
                CurrentPage=Var.TicketNew;
                btnNew.setBackgroundResource(R.drawable.buttonselected);
                btnAssign.setBackgroundResource(R.drawable.buttonunselected2);
                Page=Var.TicketNew+","+LoginActivity.username+","+LoginActivity.usertype;
            }
            else {
                CurrentPage=Var.TicketACK;
                btnNew.setBackgroundResource(R.drawable.buttonunselected2);
                btnAssign.setBackgroundResource(R.drawable.buttonselected);
                Page=Var.TicketACK+","+LoginActivity.username+","+LoginActivity.usertype;
            }

            LoadZone(Line);
        }
    }

    @Override
    public void onDestroy(){
        Log.d("test","IMDestory");
        MyConfig.TaskListMachineByLine.removeCallbacks(UpdateMachine);
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_machine__line);

        Back = (Button) findViewById(R.id.Back);
        lineName = (TextView) findViewById(R.id.lineName);
        btnNew = (Button) findViewById(R.id.btnNew);
        btnAssign = (Button) findViewById(R.id.btnAssign);
        layoutMachineList = (TableLayout) findViewById(R.id.layoutMachineList);
        LoadActivity = new LoadingClass(this,1);

        btnNew.setOnClickListener(this);
        btnAssign.setOnClickListener(this);
        Back.setOnClickListener(this);

       // String Line = getIntent().getStringExtra("_tag");
        FactoryName = getIntent().getStringExtra("FactoryName");
        Page = getIntent().getStringExtra("Page");
        Line = getIntent().getStringExtra("LineName");

        lineName.setText("LINE NAME : "+Line);
        alertHelper = new AlertHelper();

        if(Page.contains(Var.TicketNew)){
            CurrentPage=Var.TicketNew;
            btnNew.setBackgroundResource(R.drawable.buttonselected);
            btnAssign.setBackgroundResource(R.drawable.buttonunselected2);
        }
        else {
            CurrentPage=Var.TicketACK;
            btnNew.setBackgroundResource(R.drawable.buttonunselected2);
            btnAssign.setBackgroundResource(R.drawable.buttonselected);
        }

        //TaskListMachineByLine
        if(savedInstanceState==null)
        {
            CreateState=true;
            MyConfig.TaskListMachineByLine = new Handler();
            //MyConfig.TaskListMachineByLine.post(UpdateMachine);
        }

        LoadZone(Line);
    }


    private Runnable UpdateMachine = new Runnable() {
        @Override
        public void run() {
             //dofunction
            Log.d("Respnce","LoadZone responded");
            MyConfig.TaskListMachineByLine.postDelayed(UpdateMachine, 3000);
        }

    };



    private void LoadZone(String LineName) {

        RestCrud = APIClient.getClient().create(RestCrud.class);


        Call<List<AlertModel.ZoneList>> call = RestCrud.GetZone(LineName+','+Page);

        LoadActivity.show();

        call.enqueue(new Callback<List<AlertModel.ZoneList>>() {


            @SuppressLint("WrongConstant")
            @Override
            public void onResponse(Call<List<AlertModel.ZoneList>> call, Response<List<AlertModel.ZoneList>> response) {

                LoadActivity.dismiss();
                List<AlertModel.ZoneList> resource = response.body();


                int numofdata = resource.size();

                ClearTable();

                if (numofdata > 0) {

                    int remainder = 0;
                    int rownum = 0;
                    int index = numofdata % 4;
                    int toloop = 0;

                    if (numofdata <= 4) {
                        toloop = numofdata;
                        rownum = 1;
                    } else if (index == 0) {
                        toloop = 4;
                        rownum = numofdata / 4;
                    } else {
                        toloop = 4;
                        rownum = numofdata / 4;
                        remainder = numofdata - (4 * rownum);
                    }

                    int pos = 0;

                    for (int j = 0; j < rownum; j++) {
                        TableRow row = new TableRow(Machine_LineActivity.this);
                        for (int i = 0; i < toloop; i++) {
                            String ZoneId = resource.get(pos).ZoneId;
                            String ZoneName = resource.get(pos).ZoneName;
                            String DownCount = resource.get(pos).DownCount;
                            String _Color = resource.get(pos).Color;

                            Button btn = new Button(Machine_LineActivity.this);

                            int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 100, getResources().getDisplayMetrics());
                            TableRow.LayoutParams lp = new TableRow.LayoutParams(px, px);
                            lp.setMargins(30, 15, 25, 0);

                            btn.setBackgroundColor( Color.parseColor("#388E3C"));

                            if(_Color == null){

                            }
                            else if(_Color.toLowerCase().equals("yellow")){
                                btn.setBackgroundColor( Color.parseColor("#FFA000")); // From android.graphics.Color


                            }
                            else  if(_Color.toLowerCase().equals("green")){
                                btn.setBackgroundColor( Color.parseColor("#388E3C"));  // From android.graphics.Color
                            }
                            else  if(_Color.toLowerCase().equals("red")){
                                btn.setBackgroundColor(Color.RED); // From android.graphics.Color
                            }

                            btn.setTextColor(Color.WHITE); //SET CUSTOM COLOR
                            btn.setLayoutParams(lp);
                            btn.setText(ZoneName+"\n"+DownCount);
                            //btn.setTag(alert + "," + Status + "," + DateTime + "," + LineName + "," + MachineId + "," + StartDate + "," + Id);
                            btn.setTag(ZoneName);
                            btn.setOnClickListener(Machine_LineActivity.this);

                            row.addView(btn);
                            pos++;
                        }
                        layoutMachineList.addView(row);
                    }

                    if (remainder != 0) {

                        TableRow row = new TableRow(Machine_LineActivity.this);
                        for (int i = 0; i < remainder; i++) {
                            String ZoneId = resource.get(pos).ZoneId;
                            String ZoneName = resource.get(pos).ZoneName;
                            String DownCount = resource.get(pos).DownCount;
                            String _Color = resource.get(pos).Color;


                            Button btn = new Button(Machine_LineActivity.this);

                            int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 100, getResources().getDisplayMetrics());
                            TableRow.LayoutParams lp = new TableRow.LayoutParams(px, px);
                            lp.setMargins(30, 15, 25, 0);

                            if(_Color == null){

                            }

                            else if(_Color.toLowerCase().equals("yellow")){
                                btn.setBackgroundColor( Color.parseColor("#FFA000")); // From android.graphics.Color


                            }
                            else  if(_Color.toLowerCase().equals("green")){
                                btn.setBackgroundColor( Color.parseColor("#388E3C"));  // From android.graphics.Color
                            }
                            else  if(_Color.toLowerCase().equals("red")){
                                btn.setBackgroundColor(Color.RED); // From android.graphics.Color
                            }
                           /* if (Status.equals("LOCK")) {
                                btn.setBackgroundColor(Color.RED); // From android.graphics.Color

                                ObjectAnimator anim = ObjectAnimator.ofInt(btn, "backgroundColor", Color.RED, Color.parseColor("#a30000"), Color.RED);
                                anim.setDuration(1000);
                                anim.setEvaluator(new ArgbEvaluator());
                                anim.setRepeatMode(Animation.REVERSE);
                                anim.setRepeatCount(Animation.INFINITE);
                                anim.start();
                            }
                            else
                                btn.setBackgroundColor(Color.BLUE); // From android.graphics.Color*/

                            btn.setTextColor(Color.WHITE); //SET CUSTOM COLOR
                            btn.setLayoutParams(lp);
                            btn.setText(ZoneName+"\n"+DownCount);
                            //btn.setTag(alert + "," + Status + "," + DateTime + "," + LineName + "," + MachineId + "," + StartDate + "," +Id);
                            btn.setTag(ZoneName);
                            btn.setOnClickListener(Machine_LineActivity.this);

                            row.addView(btn);
                            pos++;
                        }
                        layoutMachineList.addView(row);


                    }
                }
                else if (resource.contains("false:")){
                    Toast.makeText(Machine_LineActivity.this,"(Code 110) Error :"+resource.toString(),Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<AlertModel.ZoneList>> call, Throwable t) {
                LoadActivity.dismiss();
                Toast.makeText(Machine_LineActivity.this,"(Code 110) Error :"+t.getMessage().toString(),Toast.LENGTH_SHORT).show();
            }


            private void ClearTable() {
                layoutMachineList.removeAllViews();
            }


        });


    }



    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.Back:
                myConfig.globalpage=CurrentPage;
                finish();
                break;

            case R.id.btnNew:
                if(CurrentPage.equals(Var.TicketNew)){
                    return;
                }
                CurrentPage= Var.TicketNew;
                Page= Var.TicketNew+","+LoginActivity.username+","+LoginActivity.usertype;
                btnNew.setBackgroundResource(R.drawable.buttonselected);
                btnAssign.setBackgroundResource(R.drawable.buttonunselected2);
                LoadZone(Line);
                break;

            case R.id.btnAssign:
                if(CurrentPage.equals(Var.TicketACK)){
                    return;
                }
                CurrentPage= Var.TicketACK;
                Page= Var.TicketACK+","+LoginActivity.username+","+LoginActivity.usertype;
                btnNew.setBackgroundResource(R.drawable.buttonunselected2);
                btnAssign.setBackgroundResource(R.drawable.buttonselected);
                LoadZone(Line);
                break;

            default:
                Button b = (Button) view;
                String buttonText = b.getText().toString();
                String tag = b.getTag().toString();


                Intent clickM = new Intent(Machine_LineActivity.this, ZoneListActivity.class);
                clickM.putExtra("Page",Page);
                clickM.putExtra("Zone", tag);
                clickM.putExtra("Line", Line);
                MyConfig.TaskListMachineByLine.removeCallbacks(UpdateMachine);
                CreateState=false;
                startActivity(clickM);
                break;
        }

    }


}