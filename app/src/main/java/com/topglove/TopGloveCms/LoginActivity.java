package com.topglove.TopGloveCms;

import android.annotation.SuppressLint;
import android.app. FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.topglove.TopGloveCms.Helper.ConfirmDialog;
import com.topglove.TopGloveCms.Model.AlertModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.topglove.TopGloveCms.SettingActivity.MyPREFERENCES;
import static com.topglove.TopGloveCms.SettingActivity.URL;

public class LoginActivity extends AppCompatActivity {

    public static String username;
    public static  String usertype,userposition;
    TextView tvUsername,tvPassword,tvVersion;
    Button login;
    RestCrud RestCrud;
    SharedPreferences sharedpreferences;

    @Override
    public void onResume()
    {
        super.onResume();
        tvUsername.setText("");
        tvPassword.setText("");

        try{
            APIClient.URL = "http://"+sharedpreferences.getString(URL, null).toString()+"/";
        }
        catch (Exception ex){
            SharedPreferences.Editor editor = sharedpreferences.edit();
            String restoredText = sharedpreferences.getString(URL, null);

            editor.putString(URL, "URL");
            editor.commit();
            APIClient.URL = "http://"+sharedpreferences.getString(URL, null).toString()+"/";
        }
    }

    @Override
    public void onBackPressed() {

        final ConfirmDialog cdd=new ConfirmDialog(LoginActivity.this,"Confirm Exit?");

        cdd.show();

        cdd.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(final DialogInterface arg0) {
                int stat = cdd.isOkpressed();

                if(stat==1){
                    finish();
                }
                else{

                }
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);


        setContentView(R.layout.activity_login);
        login = (Button) findViewById(R.id.login);
        tvUsername = (TextView) findViewById(R.id.username);
        tvPassword = (TextView) findViewById(R.id.password);
        tvVersion= (TextView) findViewById(R.id.tvVersion);
        final Button btn_setting = (Button) findViewById(R.id.setting);

        tvVersion.setText("Ver:"+BuildConfig.VERSION_NAME);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);



        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                username = tvUsername.getText().toString();
                AuthenticateUser(username,tvPassword.getText().toString());

            }
        });

        btn_setting.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Toast.makeText(LoginActivity.this, "Please wait loading data....", Toast.LENGTH_LONG).show();
                Intent clicksetting = new Intent(LoginActivity.this, SettingActivity.class);
                startActivity(clicksetting);


                //DialogTicketClose newFragment = new DialogTicketClose();
                //newFragment.setTicketId("64");
                //newFragment.show(getSupportFragmentManager(),"P");





            }
        });

    }

    private void AuthenticateUser(String Username,String Password) {

        //DataLoading = true;
        RestCrud = APIClient.getClient().create(RestCrud.class);
        AlertModel.UserInfo userInfo = new AlertModel().new UserInfo();

        userInfo.Username = Username;
        userInfo.Password = Password;

        login.setEnabled(false);
        Call<String> call = RestCrud.GetUser(userInfo);

        call.enqueue(new Callback<String>() {

            @SuppressLint("WrongConstant")
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
//
                login.setEnabled(true);
                String resource = response.body();

                if(resource.contains("Page Not Found")){
                    Toast.makeText(LoginActivity.this,"Error:Unable to Locate Url (URL Configured?)",Toast.LENGTH_LONG).show();
                    return;
                }

                else if(!resource.equals("0"))
                {
                    String[] data = resource.split(",");
                    usertype = data[0];
                    userposition = data[1];

                    Log.d("LoginActivity",resource);

                    Intent clicklogin = new Intent(LoginActivity.this,LineActivity.class);
                    //Intent clicklogin = new Intent(LoginActivity.this,DialogTicketClose.class);

                    MyConfig myConfig = MyConfig.getInstance();

                    myConfig.setCurrentFactoryId("1");
                    myConfig.setCurrentFactoryName("f29");


                    myConfig.setNewAlertPrev(0);
                    myConfig.setNewAlertCurrent(0);
                    myConfig.setAlertPrev(0);
                    myConfig.setAlertCurrent(0);
                    startActivity(clicklogin);
                }
                else if(resource.contains("false")){

                }
                else{
                    Toast.makeText(LoginActivity.this, "Invalid Username/Password", Toast.LENGTH_SHORT).show();
                }
            }


            @Override
            public void onFailure(Call<String> call, Throwable t) {
                login.setEnabled(true);
                Toast.makeText(LoginActivity.this, "Error Connecting to Server (Code:100)", Toast.LENGTH_LONG).show();
                //DataLoading=false;
            }

        });


    }




}

