package com.topglove.TopGloveCms;

import android.content.Intent;
import android.sax.StartElementListener;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.topglove.TopGloveCms.Helper.LoadingClass;
import com.topglove.TopGloveCms.Model.AlertModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewTicket extends AppCompatActivity {

    Spinner spinnerFactory,spinnerLine,spinnerZone,spinnerTicketModule,spinnerTciketType,spinnerChecklist,pic;
    Button save,btnBack;
    EditText targetDuration,remarks;
    CheckBox mechanical,electrical,burner,maintenance,chlorine;
    RadioGroup radioGroup_Priority,radioGroup_lineStop;
    RestCrud RestCrud;
    LoadingClass LoadActivity;

    ArrayList<String> FactoryArray = new ArrayList<String>();
    ArrayList<String> LineArray = new ArrayList<String>();
    ArrayList<String> ZoneArray = new ArrayList<String>();
    ArrayList<String> TicketModuleArray = new ArrayList<String>();
    ArrayList<String> TicketTypeArray = new ArrayList<String>();
    ArrayList<String> ChecklistArray = new ArrayList<String>();
    ArrayList<String> PicArray = new ArrayList<String>();










    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_ticker);

        LoadActivity = new LoadingClass(this,1);


        spinnerFactory = (Spinner) findViewById(R.id.spinnerFactory);
        spinnerLine = (Spinner) findViewById(R.id.spinnerLine);
        spinnerZone = (Spinner) findViewById(R.id.spinnerZone);
        spinnerTicketModule = (Spinner) findViewById(R.id.spinnerTicketModule);
        spinnerTciketType = (Spinner) findViewById(R.id.spinnerTciketType);
        spinnerChecklist = (Spinner) findViewById(R.id.spinnerChecklist);
        pic = (Spinner) findViewById(R.id.pic);

        save = (Button) findViewById(R.id.save);
        btnBack = (Button) findViewById(R.id.btnBack);

        targetDuration = (EditText) findViewById(R.id.targetDuration);
        remarks = (EditText) findViewById(R.id.remarks);

        mechanical = (CheckBox) findViewById(R.id.mechanical);
        electrical = (CheckBox) findViewById(R.id.electrical);
        burner = (CheckBox) findViewById(R.id.burner);
        maintenance = (CheckBox) findViewById(R.id.maintenance);
        chlorine = (CheckBox) findViewById(R.id.chlorine);

        radioGroup_Priority = (RadioGroup) findViewById(R.id.radioGroup_Priority);
        radioGroup_lineStop = (RadioGroup) findViewById(R.id.radioGroup_lineStop);

        LoadFactory();
        LoadTicketModule();
        LoadPic();

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(NewTicket.this,LineActivity.class);
                startActivity(intent);
            }
        });

        spinnerFactory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                LoadLine(spinnerFactory.getSelectedItem().toString());


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        spinnerLine.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                LoadZone(spinnerLine.getSelectedItem().toString());


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        spinnerTicketModule.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                LoadTicketType(spinnerTicketModule.getSelectedItem().toString());
                LoadChecklist(spinnerTicketModule.getSelectedItem().toString());


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((spinnerFactory.getSelectedItem()== null) ||(spinnerLine.getSelectedItem()== null)||(spinnerZone.getSelectedItem()== null) || (spinnerTicketModule.getSelectedItem()== null) || (spinnerTciketType.getSelectedItem()== null) || (spinnerChecklist.getSelectedItem()== null) || (pic.getSelectedItem()== null)
                        || (targetDuration.getText().toString().isEmpty()) || (remarks.getText().toString().isEmpty()) || (radioGroup_Priority.getCheckedRadioButtonId()== -1) || (radioGroup_lineStop.getCheckedRadioButtonId()== -1) )
                {
                    Toast.makeText(NewTicket.this, "Please Select Everything !!!", Toast.LENGTH_LONG).show();
                }
            }
        });



    }

    private  void LoadFactory()
    {
        RestCrud = APIClient.getClient().create(RestCrud.class);

        LoadActivity.show();
        Call<List<AlertModel.FactoryList>> call = RestCrud.GetFactory();

        call.enqueue(new Callback<List<AlertModel.FactoryList>>() {
            @Override
            public void onResponse(Call<List<AlertModel.FactoryList>> call, Response<List<AlertModel.FactoryList>> response) {
                LoadActivity.dismiss();
                List<AlertModel.FactoryList> resource = response.body();

                if(resource!=null || resource.size() != 0 ){
                    for (int i=0;i<resource.size();i++){
                        String factoryName = resource.get(i).FactoryName;
                        FactoryArray.add(factoryName);
                    }
                    String[] arraySpinner = new String[FactoryArray.size()];
                    for (int i=0;i<FactoryArray.size();i++){
                        arraySpinner[i]=FactoryArray.get(i);
                    }

                    SetAdap(arraySpinner);

                }

            }

            @Override
            public void onFailure(Call<List<AlertModel.FactoryList>> call, Throwable t) {

                Toast.makeText(NewTicket.this, "ERROR LOADING FACTORY!", Toast.LENGTH_SHORT).show();

            }
        });


    }

    private  void LoadTicketModule()
    {
        RestCrud = APIClient.getClient().create(RestCrud.class);

        LoadActivity.show();
        Call<List<AlertModel.ZoneTicket>> call = RestCrud.GetModule();

        call.enqueue(new Callback<List<AlertModel.ZoneTicket>>() {
            @Override
            public void onResponse(Call<List<AlertModel.ZoneTicket>> call, Response<List<AlertModel.ZoneTicket>> response) {
                LoadActivity.dismiss();
                List<AlertModel.ZoneTicket> resource = response.body();

                if(resource!=null || resource.size() != 0 ){
                    for (int i=0;i<resource.size();i++){
                        String ModuleName = resource.get(i).ModuleName;
                        TicketModuleArray.add(ModuleName);
                    }
                    String[] arraySpinner = new String[TicketModuleArray.size()];
                    for (int i=0;i<TicketModuleArray.size();i++){
                        arraySpinner[i]=TicketModuleArray.get(i);
                    }

                    SetAdap4(arraySpinner);

                }

            }

            @Override
            public void onFailure(Call<List<AlertModel.ZoneTicket>> call, Throwable t) {

                Toast.makeText(NewTicket.this, "ERROR LOADING TICKET MODULE!", Toast.LENGTH_SHORT).show();

            }
        });


    }

    private  void LoadPic()
    {
        RestCrud = APIClient.getClient().create(RestCrud.class);

        LoadActivity.show();
        Call<List<AlertModel.ModelWorkers>> call = RestCrud.GetPic();

        call.enqueue(new Callback<List<AlertModel.ModelWorkers>>() {
            @Override
            public void onResponse(Call<List<AlertModel.ModelWorkers>> call, Response<List<AlertModel.ModelWorkers>> response) {
                LoadActivity.dismiss();
                List<AlertModel.ModelWorkers> resource = response.body();

                if(resource!=null || resource.size() != 0 ){
                    for (int i=0;i<resource.size();i++){
                        String PIC = resource.get(i).FullName;
                        PicArray.add(PIC);
                    }
                    String[] arraySpinner = new String[PicArray.size()];
                    for (int i=0;i<PicArray.size();i++){
                        arraySpinner[i]=PicArray.get(i);
                    }

                    SetAdap7(arraySpinner);

                }

            }

            @Override
            public void onFailure(Call<List<AlertModel.ModelWorkers>> call, Throwable t) {

                Toast.makeText(NewTicket.this, "ERROR LOADING PIC!", Toast.LENGTH_SHORT).show();

            }
        });


    }

    private  void SetAdap(String[] arraySpinner1){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item, arraySpinner1);
        adapter.setDropDownViewResource(R.layout.spinner_item);
        spinnerFactory.setAdapter(adapter);

    }

    private  void SetAdap2(String[] arraySpinner1){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item, arraySpinner1);
        adapter.setDropDownViewResource(R.layout.spinner_item);

        spinnerLine.setAdapter(adapter);

    }

    private  void SetAdap3(String[] arraySpinner1){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item, arraySpinner1);
        adapter.setDropDownViewResource(R.layout.spinner_item);

        spinnerZone.setAdapter(adapter);

    }

    private  void SetAdap4(String[] arraySpinner1){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item, arraySpinner1);
        adapter.setDropDownViewResource(R.layout.spinner_item);

        spinnerTicketModule.setAdapter(adapter);

    }

    private  void SetAdap5(String[] arraySpinner1){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item, arraySpinner1);
        adapter.setDropDownViewResource(R.layout.spinner_item);

        spinnerTciketType.setAdapter(adapter);

    }

    private  void SetAdap6(String[] arraySpinner1){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item, arraySpinner1);
        adapter.setDropDownViewResource(R.layout.spinner_item);

        spinnerChecklist.setAdapter(adapter);

    }

    private  void SetAdap7(String[] arraySpinner1){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item, arraySpinner1);
        adapter.setDropDownViewResource(R.layout.spinner_item);

        pic.setAdapter(adapter);

    }

    private  void LoadLine(String FactoryName)
    {
        spinnerLine.setAdapter(null);
        LineArray.clear();
        RestCrud = APIClient.getClient().create(RestCrud.class);
        LoadActivity.show();
        Call<List<AlertModel.LineList>> call = RestCrud.GetAllLine(FactoryName);
        call.enqueue(new Callback<List<AlertModel.LineList>>() {

            @Override
            public void onResponse(Call<List<AlertModel.LineList>> call, Response<List<AlertModel.LineList>> response) {
                LoadActivity.dismiss();
                List<AlertModel.LineList> resource = response.body();

                if(resource!=null){
                    for (int i=0;i<resource.size();i++){
                        LineArray.add(resource.get(i).LineName);

                    }
                    String[] arraySpinner = new String[LineArray.size()];
                    for (int i=0;i<LineArray.size();i++){
                        arraySpinner[i]=LineArray.get(i);
                    }

                    SetAdap2(arraySpinner);

                }
                Toast.makeText(NewTicket.this,"Line Loaded" , Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<List<AlertModel.LineList>> call, Throwable t) {
                Toast.makeText(NewTicket.this, "ERROR LOADING LINE!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private  void LoadTicketType(String ModuleName)
    {
        spinnerTciketType.setAdapter(null);
        TicketTypeArray.clear();
        RestCrud = APIClient.getClient().create(RestCrud.class);
        LoadActivity.show();
        Call<List<AlertModel.ZoneTicket>> call = RestCrud.GetTicketType(ModuleName);
        call.enqueue(new Callback<List<AlertModel.ZoneTicket>>() {

            @Override
            public void onResponse(Call<List<AlertModel.ZoneTicket>> call, Response<List<AlertModel.ZoneTicket>> response) {
                LoadActivity.dismiss();
                List<AlertModel.ZoneTicket> resource = response.body();

                if(resource!=null){
                    for (int i=0;i<resource.size();i++){
                        TicketTypeArray.add(resource.get(i).TicketName);

                    }
                    String[] arraySpinner = new String[TicketTypeArray.size()];
                    for (int i=0;i<TicketTypeArray.size();i++){
                        arraySpinner[i]=TicketTypeArray.get(i);
                    }

                    SetAdap5(arraySpinner);

                }
                Toast.makeText(NewTicket.this,"Ticket Type Loaded" , Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<List<AlertModel.ZoneTicket>> call, Throwable t) {
                Toast.makeText(NewTicket.this, "ERROR LOADING TICKET TYPE!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private  void LoadChecklist(String ModuleName)
    {
        spinnerChecklist.setAdapter(null);
        ChecklistArray.clear();
        RestCrud = APIClient.getClient().create(RestCrud.class);
        LoadActivity.show();
        Call<List<AlertModel.Checklist>> call = RestCrud.GetAllCheckList(ModuleName);
        call.enqueue(new Callback<List<AlertModel.Checklist>>() {

            @Override
            public void onResponse(Call<List<AlertModel.Checklist>> call, Response<List<AlertModel.Checklist>> response) {
                LoadActivity.dismiss();
                List<AlertModel.Checklist> resource = response.body();

                if(resource!=null){
                    for (int i=0;i<resource.size();i++){
                        ChecklistArray.add(resource.get(i).CheckListName);

                    }
                    String[] arraySpinner = new String[ChecklistArray.size()];
                    for (int i=0;i<ChecklistArray.size();i++){
                        arraySpinner[i]=ChecklistArray.get(i);
                    }

                    SetAdap6(arraySpinner);

                }
                Toast.makeText(NewTicket.this,"Checklist Loaded" , Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<List<AlertModel.Checklist>> call, Throwable t) {
                Toast.makeText(NewTicket.this, "ERROR LOADING CHECKLIST!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private  void LoadZone(String LineName)
    {
        spinnerZone.setAdapter(null);
        ZoneArray.clear();
        RestCrud = APIClient.getClient().create(RestCrud.class);
        LoadActivity.show();
        Call<List<AlertModel.ZoneList>> call = RestCrud.GetAllZone(LineName);
        call.enqueue(new Callback<List<AlertModel.ZoneList>>() {

            @Override
            public void onResponse(Call<List<AlertModel.ZoneList>> call, Response<List<AlertModel.ZoneList>> response) {
                LoadActivity.dismiss();
                List<AlertModel.ZoneList> resource = response.body();

                if(resource!=null){
                    for (int i=0;i<resource.size();i++){
                        ZoneArray.add(resource.get(i).ZoneName);

                    }
                    String[] arraySpinner = new String[ZoneArray.size()];
                    for (int i=0;i<ZoneArray.size();i++){
                        arraySpinner[i]=ZoneArray.get(i);
                    }

                    SetAdap3(arraySpinner);

                }
                Toast.makeText(NewTicket.this,"Zone Loaded" , Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<List<AlertModel.ZoneList>> call, Throwable t) {
                Toast.makeText(NewTicket.this, "ERROR LOADING ZONE!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
