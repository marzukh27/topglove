package com.topglove.TopGloveCms;

import android.content.Context;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;
import android.util.Log;

/**
 * Created by X-PC on 19/2/2018.
 */

public class AlertHelper {

    public void showNotification(Context context) {

        Log.d("here", "noticationon");
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Ringtone r = RingtoneManager.getRingtone(context, notification);
        r.play();
    }

    public void StartVibrate(Context context) {
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Vibrator vibrator;
        vibrator = (Vibrator)context.getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(800);
        Ringtone r = RingtoneManager.getRingtone(context, notification);
        r.play();
    }




}
