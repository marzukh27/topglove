package com.topglove.TopGloveCms;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.topglove.TopGloveCms.Helper.LoadingClass;
import com.topglove.TopGloveCms.Helper.Var;
import com.topglove.TopGloveCms.Model.AlertModel;
import com.topglove.TopGloveCms.Model.JsonModel3;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LineActivity extends AppCompatActivity implements View.OnClickListener {


    TableLayout layoutMachineList;
    RestCrud RestCrud;
    EditText text_username;
    Button btnNew,btnAssign,btnLogout,btnApproval,btnTicket;
    TextView tvLastUpdate,displayuserid,txtFactoryName;
    TextView wifi,txtWifiSSid ;
    MyConfig myConfig = MyConfig.getInstance();
    AlertHelper alertHelper;
    String Page="NEW";
    String Pic="";

    boolean FirstLoad=false;

    private TextView battery;

    boolean isFromCreated=false;

    boolean DataLoading = false;
    LoadingClass LoadActivity;

    private BroadcastReceiver batteryinfo = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL,0);
            battery.setText(String.valueOf(level)+"%");

            if (level>=0 && level<=25)
                battery.setCompoundDrawablesWithIntrinsicBounds(R.drawable.battery1, 0, 0, 0);
            else if(level>=26 && level<=50)
                battery.setCompoundDrawablesWithIntrinsicBounds(R.drawable.battery2, 0, 0, 0);
            else if(level>=51 && level<=75)
                battery.setCompoundDrawablesWithIntrinsicBounds(R.drawable.battery3, 0, 0, 0);
            else if(level>=71 && level<=100)
                battery.setCompoundDrawablesWithIntrinsicBounds(R.drawable.battery4, 0, 0, 0);
        }
    };

    @Override
    public void onResume()
    {
        super.onResume();

        wifi();
        if(isFromCreated)
        {
            isFromCreated=false;
        }
        else
        {
            myConfig = MyConfig.getInstance();

            if(myConfig.globalpage.equals(Var.TicketNew)){
                Pic=Var.TicketNew+LoginActivity.username+","+LoginActivity.usertype;;
                Page= Var.TicketACK;
                btnNew.performClick();
            }
            else{
                Pic=Var.TicketACK+","+LoginActivity.username+","+LoginActivity.usertype;;
                Page= Var.TicketNew;
                btnAssign.performClick();
            }
        }

        Log.d("PMSTAT","Resume:  "+Pic);
    }

    @Override
    protected void onDestroy() {
        MyConfig.TaskListAllLine.removeCallbacks(UpdateLine);
        unregisterReceiver(batteryinfo);
        super.onDestroy();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_line);


        tvLastUpdate = (TextView) findViewById(R.id.tvLastUpdate);

        layoutMachineList = (TableLayout) findViewById(R.id.layoutMachineList);
        btnNew = (Button) findViewById(R.id.btnNew);
        btnAssign = (Button) findViewById(R.id.btnAssign);
        btnApproval= (Button) findViewById(R.id.btnApproval);
        btnLogout= (Button) findViewById(R.id.btnLogout);
        btnTicket = (Button) findViewById(R.id.btnTicket);
        battery = (TextView) findViewById(R.id.battery);
        wifi = (TextView) findViewById(R.id.wifi);
        txtWifiSSid = (TextView) findViewById(R.id.txtWifiSSid);
        txtFactoryName= (TextView) findViewById(R.id.txtFactory);
        displayuserid = (TextView) findViewById(R.id.displayuserid);
        txtWifiSSid.setText("");
        txtFactoryName.setText("Factory:" +myConfig.getCurrentFactoryName());

        LoadActivity = new LoadingClass(this,1);
        displayuserid.setText("Login As:"+LoginActivity.username);
        this.registerReceiver(this.batteryinfo,new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

        alertHelper = new AlertHelper();

        //if user not manager hide approval button
        if(!LoginActivity.userposition.equals(Var.Manager)){
            btnApproval.setVisibility(View.INVISIBLE);
        }

        //first time loading get new line
        Pic=Var.TicketNew+","+LoginActivity.username+","+LoginActivity.usertype;
        btnNew.setBackgroundResource(R.drawable.buttonselected);
        btnAssign.setBackgroundResource(R.drawable.buttonunselected2);

        //first time excute this create new handlder
        if(savedInstanceState==null)
        {
            GetLine();
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            MyConfig.TaskListAllLine = new Handler();
            MyConfig.TaskListAllLine.post(UpdateLine);
            myConfig.setAlertPrev(0);
        }
        myConfig.setNewAlertPrev(-1);


        btnNew.setOnClickListener(this);
        btnAssign.setOnClickListener(this);
        btnLogout.setOnClickListener(this);
        btnApproval.setOnClickListener(this);
        btnTicket.setOnClickListener(this);
        isFromCreated = true;

        wifi();


        //btnNew.performClick();

    }

    public void wifi(){

        WifiManager wifiManager = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        int numberOfLevels = 5;
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        int level = WifiManager.calculateSignalLevel(wifiInfo.getRssi(), numberOfLevels);
        int rssi = wifiInfo.getRssi();
        txtWifiSSid.setText(wifiInfo.getSSID());
        int percentage = (int) ((level/numberOfLevels)*100);

        if (rssi>=-20 && rssi<=-1)
            wifi.setText("100%");
        else if(rssi<=-51 && rssi>=-55)
            wifi.setText("90%");
        else if(rssi<=-56 && rssi>=-62)
            wifi.setText("80%");
        else if(rssi<=-63 && rssi>=-65)
            wifi.setText("75%");
        else if(rssi<=-66 && rssi>=-68)
            wifi.setText("70%");
        else if(rssi<=-69 && rssi>=-60)
            wifi.setText("60%");
        else if(rssi<=-75 && rssi>=-79)
            wifi.setText("50%");
        else if(rssi<=-80 && rssi>=-83)
            wifi.setText("30%");

        Log.d("wifi", "wifi: "+level);
        Log.d("wifi", "wifi: "+wifiInfo.getRssi());

        switch (level)
        {
            case 0: //wifi.setBackgroundResource(R.drawable.ic_signal_wifi_0_bar_teal_a700_24dp);
                wifi.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_signal_wifi_0_bar_teal_a700_24dp, 0, 0, 0);

                break;

            case 1: //wifi.setBackgroundResource(R.drawable.ic_signal_wifi_1_bar_teal_700_24dp);
                wifi.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_signal_wifi_1_bar_teal_700_24dp, 0, 0, 0);

                break;

            case 2: //wifi.setBackgroundResource(R.drawable.ic_signal_wifi_2_bar_teal_700_24dp);
                wifi.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_signal_wifi_2_bar_teal_700_24dp, 0, 0, 0);

                break;

            case 3: //wifi.setBackgroundResource(R.drawable.ic_signal_wifi_3_bar_teal_700_24dp);
                wifi.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_signal_wifi_3_bar_teal_700_24dp, 0, 0, 0);

                break;

            case 4: //wifi.setBackgroundResource(R.drawable.ic_signal_wifi_4_bar_teal_700_24dp);
                wifi.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_signal_wifi_4_bar_teal_700_24dp, 0, 0, 0);

                break;
        }


    }

    private Runnable UpdateLine = new Runnable() {
        @Override
        public void run() {

            if(DataLoading==false)
            {
                CheckNewDown();

                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss yyyy-MM-dd");
                String currentDateandTime = sdf.format(new Date());
                tvLastUpdate.setText("Last Update :" + currentDateandTime);
                wifi();

            }


            MyConfig.TaskListAllLine.postDelayed(UpdateLine, 3000);
        }

    };


    private void CheckNewDown()
    {
        RestCrud = APIClient.getClient().create(RestCrud.class);
        Call<String> call = RestCrud.GetNewDownMachine(LoginActivity.username +","+ LoginActivity.usertype);

        call.enqueue(new Callback<String>() {

            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if(response.body() == null){
                    return;
                }
                else if (response.body().contains("false:")){
                    Toast.makeText(LineActivity.this, "Error Check New Down"+response.body(), Toast.LENGTH_LONG).show();
                    return;
                }
                else if(DataLoading==true){
                    //previous call not finish return now
                    return;
                }

                int NumberOfData = Integer.valueOf(response.body());
                myConfig.setNewAlertCurrent(NumberOfData);

                if(myConfig.getNewAlertCurrent() != myConfig.getNewAlertPrev())
                {

                    if(Page.equals(Var.TicketNew)){
                        Pic=Var.TicketNew+","+LoginActivity.username+","+LoginActivity.usertype;
                        GetLine();
                    }
                    else if(Page.equals(Var.TicketACK)){
                        Pic = Var.TicketACK+","+LoginActivity.username+","+LoginActivity.usertype;
                        GetLine();

                    }


                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(LineActivity.this, "Error Loading Data"+t.getMessage(), Toast.LENGTH_LONG).show();

            }

        });
    }

    private void GetLine() {

        //data loading
        DataLoading=true;

        RestCrud = APIClient.getClient().create(RestCrud.class);

        Call<List<AlertModel.LineList>> call = RestCrud.GetLine(myConfig.getCurrentFactoryName()+","+Pic);

        call.enqueue(new Callback<List<AlertModel.LineList>>() {

            @SuppressLint("WrongConstant")
            @Override
            public void onResponse(Call<List<AlertModel.LineList>> call, Response<List<AlertModel.LineList>> response) {

                LoadActivity.dismiss();

                if(response.body() == null){
                    DataLoading=false;
                    return;
                }
                else if (response.body().contains("false:")){
                    DataLoading=false;
                    Toast.makeText(LineActivity.this,"Error:"+response.body(),Toast.LENGTH_SHORT).show();
                    return;
                }

                List<AlertModel.LineList> resource = response.body();

                int numofdata = resource.size();
                layoutMachineList.removeAllViews();

                if (numofdata > 0) {

                    int remainder = 0;

                    int rownum = 0;
                    int index = numofdata % 4;
                    int toloop = 0;

                    if (numofdata <= 4) {
                        toloop = numofdata;
                        rownum = 1;
                    } else if (index == 0) {
                        toloop = 4;
                        rownum = numofdata / 4;
                    } else {
                        toloop = 4;
                        rownum = numofdata / 4;
                        remainder = numofdata - (4 * rownum);
                    }

                    int pos = 0;

                    for (int j = 0; j < rownum; j++) {

                        TableRow row = new TableRow(LineActivity.this);
                        for (int i = 0; i < toloop; i++)
                        {
                            String alert = resource.get(pos).LineName;
                            String downCount = String.valueOf(resource.get(pos).DownCount);

                            String[] rowData = alert.split(Pattern.quote("|"));
                            String[] AlertMessage = rowData[0].split(Pattern.quote("~"));

                            Button btn = new Button(LineActivity.this);



                            int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 100, getResources().getDisplayMetrics());
                            TableRow.LayoutParams lp = new TableRow.LayoutParams(px, px);
                            lp.setMargins(30, 15, 25, 0);
                            //int val = 0;
                            int val = Integer.valueOf(downCount);
                            if(val>0){

                                btn.setBackgroundColor(Color.RED); // From android.graphics.Color
                                //blink
                               /* ObjectAnimator anim = ObjectAnimator.ofInt(btn,"backgroundColor",Color.RED,Color.parseColor("#a30000"),Color.RED);
                                anim.setDuration(1000);
                                anim.setEvaluator(new ArgbEvaluator());
                                anim.setRepeatMode(Animation.REVERSE);
                                anim.setRepeatCount(Animation.INFINITE);
                                anim.start();*/
                            }
                            else {
                                btn.setBackgroundColor(Color.BLUE); // From android.graphics.Color
                            }

                            btn.setTextColor(Color.WHITE); //SET CUSTOM COLOR
                            btn.setLayoutParams(lp);
                            btn.setText(alert+"\n"+downCount);
                            btn.setTag(resource.get(pos).LineName);
                            //int _downCount = Integer.valueOf(downCount);
                            //DountCount = DountCount +  _downCount;
                            btn.setOnClickListener(LineActivity.this);

                            row.addView(btn);
                            pos++;
                        }
                        layoutMachineList.addView(row);
                    }

                    if (remainder != 0) {

                        TableRow row = new TableRow(LineActivity.this);
                        for (int i = 0; i < remainder; i++) {
                            String alert = resource.get(pos).LineName;
                            String downCount = String.valueOf(resource.get(pos).DownCount);

                            String[] rowData = alert.split(Pattern.quote("|"));
                            String[] AlertMessage = rowData[0].split(Pattern.quote("~"));

                            Button btn = new Button(LineActivity.this);

                            int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 100, getResources().getDisplayMetrics());
                            TableRow.LayoutParams lp = new TableRow.LayoutParams(px, px);
                            lp.setMargins(30, 15, 25, 0);
                            int val = 0;

                            if(val>0){
                                btn.setBackgroundColor(Color.RED); // From android.graphics.Color
                                //blink
                               /* ObjectAnimator anim = ObjectAnimator.ofInt(btn,"backgroundColor",Color.RED,Color.parseColor("#a30000"),Color.RED);
                                anim.setDuration(1000);
                                anim.setEvaluator(new ArgbEvaluator());
                                anim.setRepeatMode(Animation.REVERSE);
                                anim.setRepeatCount(Animation.INFINITE);
                                anim.start();*/
                            }
                            else {
                                btn.setBackgroundColor(Color.BLUE); // From android.graphics.Color
                            }

                            btn.setTextColor(Color.WHITE); //SET CUSTOM COLOR
                            btn.setLayoutParams(lp);
                            btn.setText(alert+"\n"+downCount);
                            btn.setTag(resource.get(pos).LineName);
                            btn.setOnClickListener(LineActivity.this);

                            row.addView(btn);
                            pos++;
                        }
                        layoutMachineList.addView(row);
                    }


                    if(myConfig.getNewAlertCurrent() < myConfig.getNewAlertPrev()){

                    }
                    else if(myConfig.getNewAlertPrev() == myConfig.getNewAlertCurrent()){

                    }
                    else{
                        alertHelper.StartVibrate(LineActivity.this);
                    }

                    myConfig.setNewAlertPrev(myConfig.getNewAlertCurrent());

                    if(DataLoading==true)
                        DataLoading=false;


                }
                else{
                    if(myConfig.getNewAlertCurrent() < myConfig.getNewAlertPrev()){

                    }
                    else if(myConfig.getNewAlertPrev() == myConfig.getNewAlertCurrent()){

                    }
                    else{
                        alertHelper.StartVibrate(LineActivity.this);
                    }

                    myConfig.setNewAlertPrev(myConfig.getNewAlertCurrent());

                    if(DataLoading==true)
                        DataLoading=false;
                }



            }

            @Override
            public void onFailure(Call<List<AlertModel.LineList>> call, Throwable t) {
                LoadActivity.dismiss();

                Toast.makeText(LineActivity.this, "Error Loading Data.....", Toast.LENGTH_LONG).show();
                DataLoading=false;
            }

        });


    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.btnNew:

                if(Page.equals("NEW")){
                    return;
                }
                Page="NEW";
                layoutMachineList.removeAllViews();
                //Toast.makeText(this, "Please Wait Loading.....", Toast.LENGTH_SHORT).show();
                btnNew.setBackgroundResource(R.drawable.buttonselected);
                btnAssign.setBackgroundResource(R.drawable.buttonunselected2);
                FirstLoad=true;
                Pic= Var.TicketNew+","+LoginActivity.username+","+LoginActivity.usertype;
                GetLine();
                break;

            case R.id.btnAssign:

                if(Page.equals(Var.TicketACK)){
                    return;
                }
                Page= Var.TicketACK;
                layoutMachineList.removeAllViews();
                layoutMachineList.invalidate();
                layoutMachineList.refreshDrawableState();
                //Toast.makeText(this, "Please Wait Loading.....", Toast.LENGTH_SHORT).show();
                btnAssign.setBackgroundResource(R.drawable.buttonselected);
                btnNew.setBackgroundResource(R.drawable.buttonunselected2);
                FirstLoad=true;
                Pic = Var.TicketACK+","+LoginActivity.username+","+LoginActivity.usertype;
                LoadActivity.show();
                GetLine();
                break;

            case R.id.btnApproval:
                Intent intent = new Intent(LineActivity.this, ApproveActivity.class);
                startActivity(intent);
                break;

            case R.id.btnLogout:
                finish();
                //Toast.makeText(this, "cdd Wait Loading.....", Toast.LENGTH_SHORT).show();
                break;

            case R.id.btnTicket:
                Intent newTicket = new Intent(LineActivity.this,NewTicket.class);
                startActivity(newTicket);
                break;

            default:
                Button b = (Button) view;
                String _text = b.getText().toString();
                String _tag = b.getTag().toString();


                Intent clickM = new Intent(LineActivity.this, Machine_LineActivity.class);
                clickM.putExtra("FactoryName", _text);
                clickM.putExtra("LineName", _tag);
                clickM.putExtra("Page", Pic);
                Pic="";
                startActivity(clickM);

                /*Intent detailpage = new Intent(LineActivity.this,Detail.class);
                detailpage.putExtra("Page",Page);
                startActivity(detailpage);*/
                break;
        }

    }


}