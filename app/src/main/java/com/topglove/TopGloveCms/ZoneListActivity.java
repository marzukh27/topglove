package com.topglove.TopGloveCms;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.topglove.TopGloveCms.Helper.LoadingClass;
import com.topglove.TopGloveCms.Helper.MyCustomAdapter;
import com.topglove.TopGloveCms.Helper.MyCustomAdapterModel;
import com.topglove.TopGloveCms.Helper.Var;
import com.topglove.TopGloveCms.Model.AlertModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ZoneListActivity extends AppCompatActivity implements View.OnClickListener {

    Button Back,btnNew,btnAssign;
    //Button btnattend;
    TextView lineName;
    TableLayout layoutMachineList;
    RestCrud RestCrud;
    String Page = "";
    String CurrentPage="";
    String ZoneName="";
    String Line;
    MyConfig myConfig = MyConfig.getInstance();
    AlertHelper alertHelper;
    List<AlertModel.ModelDownTime> modelDownTimes;
    boolean CreateState=true;
    LoadingClass LoadActivity;
    boolean isForceLoad=true;

    int prevnumberofdata=0;
    public void onResume(){
        super.onResume();
        Log.d("test","ImResume");
        if(!CreateState){
            MyConfig.TaskListMachineByLine.removeCallbacks(UpdateMachine);
            MyConfig.TaskListMachineByLine.post(UpdateMachine);

        }
    }

    public void onDestroy(){
        Log.d("test","IMDestory");
        MyConfig.TaskListMachineByLine.removeCallbacks(UpdateMachine);
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_zone_list);

        Back = (Button) findViewById(R.id.Back);
        //btnattend = (Button) findViewById(R.id.btnattend);

        lineName = (TextView) findViewById(R.id.lineName);
        btnNew = (Button) findViewById(R.id.btnNew);
        btnAssign = (Button) findViewById(R.id.btnAssign);

        layoutMachineList = (TableLayout) findViewById(R.id.layoutMachineList);
        LoadActivity = new LoadingClass(this,1);



        // String Line = getIntent().getStringExtra("_tag");
        ZoneName = getIntent().getStringExtra("Zone");
        Page = getIntent().getStringExtra("Page");
        Line = getIntent().getStringExtra("Line");

        if(Page.contains(Var.TicketNew)){
            CurrentPage=Var.TicketNew;
            btnNew.setBackgroundResource(R.drawable.buttonselected);
            btnAssign.setBackgroundResource(R.drawable.buttonunselected2);
        }
        else {
            CurrentPage=Var.TicketACK;
            btnNew.setBackgroundResource(R.drawable.buttonunselected2);
            btnAssign.setBackgroundResource(R.drawable.buttonselected);
        }


        lineName.setText("ZONE NAME : "+ZoneName);


        alertHelper = new AlertHelper();

        Back.setOnClickListener(this);
        btnNew.setOnClickListener(this);
        btnAssign.setOnClickListener(this);

        //TaskListMachineByLine
        if(savedInstanceState==null)
        {
            CreateState=true;
            MyConfig.TaskListMachineByLine = new Handler();
            MyConfig.TaskListMachineByLine.post(UpdateMachine);
        }

        LoadActivity.show();
        LoadZoneTicket(ZoneName);
    }


    private Runnable UpdateMachine = new Runnable() {
        @Override
        public void run() {
            //dofunction
            Log.d("Respnce","LoadZone responded");
            LoadZoneTicket(ZoneName);
            MyConfig.TaskListMachineByLine.postDelayed(UpdateMachine, 3000);
        }

    };



    private void LoadZoneTicket(String ZoneName) {

        RestCrud = APIClient.getClient().create(RestCrud.class);

        Call<List<AlertModel.ZoneTicket>> call = RestCrud.GetZoneTicket(Line+","+ZoneName+','+Page);

        //if(prevnumberofdata==0){
           //LoadActivity.show();
        //}


        call.enqueue(new Callback<List<AlertModel.ZoneTicket>>() {


            @SuppressLint("WrongConstant")
            @Override
            public void onResponse(Call<List<AlertModel.ZoneTicket>> call, Response<List<AlertModel.ZoneTicket>> response) {

                LoadActivity.dismiss();
                List<AlertModel.ZoneTicket> resource = response.body();
                ScrollView scrollView = (ScrollView)findViewById(R.id.scrollView3);

                RecyclerView DynamicListView = new RecyclerView(ZoneListActivity.this);
                List<MyCustomAdapterModel> itemList = new ArrayList<>();

                int numofdata = resource.size();



                if (numofdata > 0) {

                    if(prevnumberofdata==0){
                        prevnumberofdata=numofdata;
                    }
                    else if((prevnumberofdata==numofdata) &&(isForceLoad==false)){
                        return;
                    }

                    isForceLoad=false;

                    for (int i = 0; i < numofdata; i++) {
                        int Id = resource.get(i).Id;
                        String LineName = resource.get(i).LineName;
                        int TicketNumber = resource.get(i).TicketNumber;
                        int Priority = resource.get(i).Priority;


                        MyCustomAdapterModel item = new MyCustomAdapterModel();

                        item.setId(Id);
                        item.setPriority(Priority);
                        item.setModuleName(resource.get(i).ModuleName);
                        item.setTicketName(resource.get(i).TicketName);
                        item.setDescription(resource.get(i).Description);
                        item.setRemedy(resource.get(i).Remedy);
                        item.setNeededApproval(resource.get(i).NeededApproval);
                        item.setRemarks(resource.get(i).Remarks);
                        item.setCode(resource.get(i).Code);
                        item.setTaskId(resource.get(i).TaskId);
                        item.setLineName(LineName);
                        item.setTicketNumber(TicketNumber);
                        item.setWorkersName(resource.get(i).WorkerName);
                        item.setWorkers(resource.get(i).Workers);

                        if(resource.get(i).Color != null || resource.get(i).equals("")){
                            item.setColor(resource.get(i).Color);
                        }
                        else {
                            item.setColor("#fe970a");
                        }


                        item.setStatus(resource.get(i).Status);
                        itemList.add(item);
                    }

                    //itemList.notify();

                    MyCustomAdapter adapter = new MyCustomAdapter(ZoneListActivity.this, itemList,CurrentPage);



                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ZoneListActivity.this);
                    DynamicListView.setLayoutManager(mLayoutManager);

                    DynamicListView.setAdapter(adapter);
                    scrollView.removeAllViews();
                    scrollView.addView(DynamicListView);

                }
                else
                {
                    scrollView.removeAllViews();
                }
            }

            @Override
            public void onFailure(Call<List<AlertModel.ZoneTicket>> call, Throwable t) {
                LoadActivity.dismiss();
                Toast.makeText(ZoneListActivity.this,"(Code 110) Error :"+t.getMessage().toString(),Toast.LENGTH_SHORT).show();
            }


            private void ClearTable() {
                layoutMachineList.removeAllViews();
            }


        });


    }



    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.Back:
                myConfig.globalpage=CurrentPage;
                finish();
                break;

            case R.id.btnNew:

                if(CurrentPage.equals(Var.TicketNew)){
                    return;
                }
                CurrentPage= Var.TicketNew;
                Page= Var.TicketNew+","+LoginActivity.username+","+LoginActivity.usertype;
                btnNew.setBackgroundResource(R.drawable.buttonselected);
                btnAssign.setBackgroundResource(R.drawable.buttonunselected2);
                isForceLoad=true;
                LoadActivity.show();
                LoadZoneTicket(ZoneName);
                break;

            case R.id.btnAssign:

                if(CurrentPage.equals(Var.TicketACK)){
                    return;
                }
                CurrentPage= Var.TicketACK;
                Page= Var.TicketACK+","+LoginActivity.username+","+LoginActivity.usertype;
                btnNew.setBackgroundResource(R.drawable.buttonunselected2);
                btnAssign.setBackgroundResource(R.drawable.buttonselected);
                isForceLoad=true;
                LoadActivity.show();
                LoadZoneTicket(ZoneName);
                break;

            default:

                break;
        }

    }


}