package com.topglove.TopGloveCms;

import android.os.Handler;

import com.topglove.TopGloveCms.Model.AlertModel;

import java.security.PublicKey;


/**
 * Created by X-PC on 19/2/2018.
 */

public class MyConfig {

    private static MyConfig instance = null;
    protected MyConfig() {
        // Exists only to defeat instantiation.
    }
    public static MyConfig getInstance() {
        if(instance == null) {
            instance = new MyConfig();
        }
        return instance;
    }


    public static Handler TaskListAllMachine;

    public static Handler TaskListAllLine;
    public static Handler TaskListMachineByLine;


    private boolean DataUpdated=false;
    public boolean isDataUpdated() {
        return DataUpdated;
    }

    public void setDataUpdated(boolean dataUpdated) {
        DataUpdated = dataUpdated;
    }


    private String CurrentFactoryName="";
    private String CurrentFactoryId="";

    public String getCurrentFactoryName() {
        return CurrentFactoryName;
    }
    public void setCurrentFactoryName(String _CurrentFactoryName) {
        CurrentFactoryName = _CurrentFactoryName;
    }

    public String getCurrentFactoryId() {
        return CurrentFactoryId;
    }
    public void setCurrentFactoryId(String _CurrentFactoryId) {
        CurrentFactoryId = _CurrentFactoryId;
    }



    private int AlertPrev=0;
    private int AlertCurrent=0;
    private int NewAlertPrev=0;
    private int NewAlertCurrent=0;


    public int getNewAlertPrev() {
        return NewAlertPrev;
    }

    public void setNewAlertPrev(int newAlertPrev) {
        NewAlertPrev = newAlertPrev;
    }

    public int getNewAlertCurrent() {
        return NewAlertCurrent;
    }

    public void setNewAlertCurrent(int newAlertCurrent) {
        NewAlertCurrent = newAlertCurrent;
    }


    public int getAlertCurrent() {
        return AlertCurrent;
    }

    public void setAlertCurrent(int alertCurrent) {
        AlertCurrent = alertCurrent;
    }

    public int getAlertPrev() {
        return AlertPrev;
    }

    public void setAlertPrev(int alertPrev) {
        AlertPrev = alertPrev;
    }


    private AlertModel.ModelDownTime modelDownTime;

    public AlertModel.ModelDownTime getModelDownTime() {
        return modelDownTime;
    }

    public void setModelDownTime(AlertModel.ModelDownTime modelDownTime) {
        this.modelDownTime = modelDownTime;
    }

    public static String globalpage;

    public String getCloseStatus() {
        return CloseStatus;
    }

    public void setCloseStatus(String closeStatus) {
        CloseStatus = closeStatus;
    }

    private String CloseStatus;


    //region ListAllMachine Variable

    //endregion
}
